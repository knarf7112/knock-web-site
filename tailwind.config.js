/** @type {import('tailwindcss').Config} */
const config = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {},
    // 修改主题色配置,与element-plus配置方式同步
    colors: {
      primary: genSimilarColorsName('primary'),
      info: genSimilarColorsName('info'),
      success: genSimilarColorsName('success'),
      warning: genSimilarColorsName('warning'),
      danger: genSimilarColorsName('danger')
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
  /** https://tailwindcss.com/docs/preflight */
  corePlugins: {
    preflight: false, // 禁止 tailwind 自动导入自己的初始化样式, 避免 Element-Plus 的 Button 样式被盖掉
  },
}

// 生成衍生颜色css变量名
function genSimilarColorsName(brandName) {
  return {
    lighter: `var(--${brandName}-lighter-color)`,
    light: `var(--${brandName}-light-color)`,
    DEFAULT: `var(--${brandName}-color)`,
    deep: `var(--${brandName}-deep-color)`,
    deeper: `var(--${brandName}-deeper-color)`
  };
}

export default config