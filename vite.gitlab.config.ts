import { fileURLToPath, URL } from 'node:url'
import { resolve, dirname } from 'node:path'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import VueI18nPlugin from '@intlify/unplugin-vue-i18n/vite'

// https://vitejs.dev/config/
export default defineConfig({
  // css: {
  //   preprocessorOptions: {
  //     scss: {
  //       additionalData: `@use "@/theme/index.scss" as *;`, // override 'element-plus' theme
  //     }
  //   }
  // },
  base: '/knock-web-site/', // for my gitlab web site that have prefix path
  server: {
    proxy: {
      '/api': {
        target: 'http://localhost:6636/',
        changeOrigin: true
      }
    }
  },
  plugins: [
    vue(),
    vueJsx(),
    AutoImport({
      resolvers: [ElementPlusResolver()] // on-demand import 'element-plus' component
    }),
    Components({
      resolvers: [
        ElementPlusResolver()
        /* override 'element-plus' theme
        {
          importStyle: "sass",
          directives: false,
        }*/
      ] // on-demand import 'element-plus' component
    }),
    VueI18nPlugin({
      /* options */
      // locale messages resource pre-compile option
      include: resolve(dirname(fileURLToPath(import.meta.url)), './src/i18n/locales/**')
    })
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
})
