import {
  RootRouteMaps,
  ManageRouteMaps,
  OperationRouteMaps,
  AdvertiseRouteMaps,
  CustomerRouteMaps
} from '@/router/constants'
import type { MainMenuItem, MenuItem } from '@/components/Menu'

const { dashboard } = RootRouteMaps.home.children
const { admin, member, permission } = ManageRouteMaps
const { bindingApplication, rightsProtectionApplication, leagueApplication } = OperationRouteMaps
const { adSettings, announcement } = AdvertiseRouteMaps
const { contactInfo } = CustomerRouteMaps

/** 公共菜单 */
const PublicMenuList: MainMenuItem[] = [
  {
    key: '0',
    title: '仪表板',
    routeTo: dashboard.name
  }
]

/** 私有菜单 */
const PrivateMenuList: MainMenuItem[] = [
  {
    key: '1',
    title: '管理',
    apiMapping: {
      p_id: -1,
      id: 1,
      title: '帐号管理'
    },
    children: [
      {
        key: '1-1',
        title: '后台用户',
        routeTo: admin.name,
        apiMapping: {
          p_id: 1,
          id: 2,
          title: '后台用户'
        }
      },
      {
        key: '1-2',
        title: '权限设置',
        routeTo: permission.name,
        apiMapping: {
          p_id: 1,
          id: 4,
          title: '权限设置'
        }
      },
      {
        key: '1-3',
        title: '一般会员',
        routeTo: member.name,
        apiMapping: {
          p_id: 1,
          id: 3,
          title: '一般会员'
        }
      }
    ]
  },
  {
    key: '2',
    title: '运营',
    apiMapping: {
      p_id: -1,
      id: 5,
      title: '运营管理'
    },
    children: [
      {
        key: '2-1',
        title: '绑定申请',
        routeTo: bindingApplication.name,
        apiMapping: {
          p_id: 5,
          id: 6,
          title: '绑定申请'
        }
      },
      {
        key: '2-2',
        title: '维权申请',
        routeTo: rightsProtectionApplication.name,
        apiMapping: {
          p_id: 5,
          id: 7,
          title: '维权申请'
        }
      },
      {
        key: '2-3',
        title: '联盟申请',
        routeTo: leagueApplication.name,
        apiMapping: {
          p_id: 5,
          id: 8,
          title: '联盟申请'
        }
      }
    ]
  },
  {
    key: '3',
    title: '广宣',
    apiMapping: {
      p_id: -1,
      id: 9,
      title: '广宣管理'
    },
    children: [
      {
        key: '3-1',
        title: '广告设置',
        routeTo: adSettings.name,
        apiMapping: {
          p_id: 9,
          id: 10,
          title: '广告设置'
        }
      },
      {
        key: '3-2',
        title: '跑马公告',
        routeTo: announcement.name,
        apiMapping: {
          p_id: 9,
          id: 11,
          title: '跑马公告'
        }
      }
    ]
  },
  {
    key: '4',
    title: '客服',
    apiMapping: {
      p_id: -1,
      id: 12,
      title: '客服管理'
    },
    children: [
      {
        key: '4-1',
        title: '联系方式',
        routeTo: contactInfo.name,
        apiMapping: {
          p_id: 12,
          id: 13,
          title: '联系方式'
        }
      }
    ]
  }
]

/** 取得所有子选单 */
const getFlatMenuItems = (items: MenuItem[]): MenuItem[] => {
  /** 碾平列表 */
  const flatMenu = (item: MenuItem): MenuItem | MenuItem[] => {
    if (!item.children?.length) {
      return item
    } else {
      return item.children.flatMap(flatMenu)
    }
  }

  return items.flatMap(flatMenu)
}

export { PublicMenuList, PrivateMenuList, getFlatMenuItems }
