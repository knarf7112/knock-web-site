import { updateThemeColorVar } from './gen-color'

type Theme = {
  colors: {
    primary?: string
    info?: string
    warning?: string
    success?: string
    danger?: string
  }
}

// 默认主题配置
export const defaultThemeConfig: Theme = {
  colors: {
    primary: '#169bd5',
    info: '#eeeeee',
    warning: '#fbbd23',
    danger: '#f87272',
    success: '#36d399'
  }
}

/** 设置主题 */
function setTheme(theme: Theme = defaultThemeConfig) {
  updateThemeColorVar(theme)
}

export { setTheme }

export type { Theme }
