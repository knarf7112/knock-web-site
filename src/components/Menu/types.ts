/** 选单项目 */
type BaseMenuItem = {
  /** 项目键值 */
  key: string
  /** 显示名称 */
  title: string
  /** 图示 */
  icon?: string
  /** 路由名称 */
  routeTo?: string
}

type MenuItem<T extends BaseMenuItem = BaseMenuItem> = T & {
  children?: T[]
}

type MenuItemWithApi = BaseMenuItem & {
  /** 后端API对照 */
  apiMapping?: {
    id: number
    p_id: number
    title: string
  }
}

/** 主选单项目 */
type MainMenuItem = MenuItem<MenuItemWithApi>

export type { MenuItem, MainMenuItem }
