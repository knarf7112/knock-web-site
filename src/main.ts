import { createApp, markRaw } from 'vue'
import { createPinia } from 'pinia'
import * as dayjs from 'dayjs'

import App from './App.vue'
import router from './router'
import i18n from './i18n'
import axios from './api/axios'
import { setTheme } from './theme'

import './assets/main.css'
import './index.css'
import 'dayjs/locale/zh-cn' // import locale
// If you want to use ElMessage, import it.
import 'element-plus/theme-chalk/src/message.scss'

dayjs.locale('zh-cn')
setTheme()

const app = createApp(App)

app.config.globalProperties.$axios = axios
app.config.errorHandler = function (err) {
  console.error('[global] catch error', err)
}

const pinia = createPinia()
pinia.use(({ store }) => {
  store.router = markRaw(router)
})

app.use(pinia)
app.use(router)
app.use(i18n)

app.mount('#app')
