import type { RouteRecordRaw } from 'vue-router'
import {
  ManageRouteMaps,
  OperationRouteMaps,
  AdvertiseRouteMaps,
  CustomerRouteMaps
} from './constants'

const { admin, member, permission } = ManageRouteMaps
const { bindingApplication, rightsProtectionApplication, leagueApplication } = OperationRouteMaps
const { adSettings, announcement } = AdvertiseRouteMaps
const { contactInfo } = CustomerRouteMaps

const privateRoutes: RouteRecordRaw[] = [
  /** 账号管理 - 后台用户 */
  {
    path: admin.path,
    name: admin.name,
    component: () => import('@/views/Manage/Admin/AdminListing.vue'),
    meta: {
      title: ''
    }
  },
  /** 账号管理 - 权限设置 */
  {
    path: permission.path,
    name: permission.name,
    component: () => import('@/views/Manage/Admin/PermissionSetting.vue')
  },
  /** 账号管理 - 一般会员 */
  {
    path: member.path,
    name: member.name,
    component: () => import('@/views/Manage/Member/MemberListing.vue')
  },
  /** 运营管理 - 绑定申请 */
  {
    path: bindingApplication.path,
    name: bindingApplication.name,
    component: () => import('@/views/Operation/BindingApplication.vue')
  },
  /** 运营管理 - 维权申请 */
  {
    path: rightsProtectionApplication.path,
    name: rightsProtectionApplication.name,
    component: () => import('@/views/Operation/RightsProtectionApplication.vue')
  },
  /** 运营管理 - 联盟申请 */
  {
    path: leagueApplication.path,
    name: leagueApplication.name,
    component: () => import('@/views/Operation/LeagueApplication.vue')
  },
  /** 广宣管理 - 广告设置 */
  {
    path: adSettings.path,
    name: adSettings.name,
    component: () => import('@/views/Advertise/AdSettings.vue')
  },
  /** 广宣管理 - 跑马公告 */
  {
    path: announcement.path,
    name: announcement.name,
    component: () => import('@/views/Advertise/AnnouncementSettings.vue')
  },
  /** 客服管理 - 联系方式 */
  {
    path: contactInfo.path,
    name: contactInfo.name,
    component: () => import('@/views/CustomerService/ContactInfo.vue')
  }
]

export { privateRoutes }
