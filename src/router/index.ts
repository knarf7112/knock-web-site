import { createRouter, createWebHistory, type RouteRecordRaw } from 'vue-router'
import { ElLoading } from 'element-plus'
import i18n, {
  loadLocaleMessage,
  setI18nLanguage,
  Language,
  DEFAULT_LANG,
  SUPPORT_LANGS
} from '@/i18n'
import { getLocalStorage } from '@/utils'
import { RootRouteMaps } from '@/router/constants'
import { useUserStore } from '@/stores'
import { getMenu } from '@/api/auth'
import { StatusCode } from '@/api/constants'
import type { Menu } from '@/api/auth/types'
import NotFound from '@/views/NotFound.vue'
import { getFlatMenuItems, PrivateMenuList } from '@/constants/mainMenu'
import type { MainMenuItem } from '@/components/Menu'
import { privateRoutes } from './privateRoutes'
import { mockMenu } from './mock'

const { login, home, about, notFound } = RootRouteMaps
const { dashboard } = RootRouteMaps.home.children

const publicRoutes: RouteRecordRaw[] = [
  {
    path: login.path,
    name: login.name,
    component: () => import('@/views/AppLogin.vue')
  },
  {
    path: home.path,
    name: home.name,
    component: () => import('@/views/HomeView.vue'),
    meta: {
      title: 'Home'
    },
    children: [
      {
        path: dashboard.path,
        name: dashboard.name,
        component: () => import('@/views/Dashboard/MainDashboard.vue')
      }
    ]
  },
  {
    path: about.path,
    name: about.name,
    // route level code-splitting
    // this generates a separate chunk (About.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('@/views/AboutView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/:lang',
      name: 'lang',
      beforeEnter: async (to) => {
        const lang = to.params.lang as Language

        if (!SUPPORT_LANGS.includes(lang)) {
          return new Error(`lang(${lang}) not support`)
        }

        // load locale messages
        if (!i18n.global.availableLocales.includes(lang)) {
          // start import lang
          await loadLocaleMessage(i18n, lang)
        }

        // set i18n language
        setI18nLanguage(i18n, lang)
        return true
      },
      children: publicRoutes
    },
    {
      path: '/',
      name: 'root',
      redirect: () => {
        const lang = getLocalStorage('lang') as Language
        return { path: `/${!lang ? DEFAULT_LANG : lang}` }
      }
    },
    {
      path: notFound.path,
      name: notFound.name,
      component: NotFound
    }
  ]
})

router.beforeEach(async (to, from) => {
  const userStore = useUserStore()

  // 若使用者未登入且当前路由非'登入', 转导登入页
  if (!userStore.user.isLogged && to.name !== login.name) {
    const lang = getLang(to.fullPath)
    return {
      path: `/${lang}/${login.path}`,
      query: {
        redirectTo: to.fullPath
      }
    }
  }

  // TODO: redirect when login
  if (to.name !== login.name && to.query.redirectTo) {
    console.log('redirect url', to.query)
    return { path: to.query.redirectTo }
  }

  // 使用者未载入私有路由
  if (userStore.user.isLogged && !userStore.user.isLoadedPrivateRoutes) {
    // clear private routes
    privateRoutes.forEach(
      (route) => router.hasRoute(route.name as string) && router.removeRoute(route.name as string)
    )

    const loading = ElLoading.service({
      lock: true,
      text: 'Loading',
      background: 'rgba(0, 0, 0, 0.7)'
    })
    //const res = await getMenu()
    const res = await new Promise<{ status: string, message: string, data: any }>((resolve) => {
      setTimeout(() => {
        resolve({
          status: '0000',
          message: 'Success',
          data: mockMenu
        })

      }, 1000)
    })

    loading.close()
    if (res.status === StatusCode.Success && Array.isArray(res.data)) {
      //const apiMenu = res.data
      const apiMenu = mockMenu
      const permittedMenu = genMenu(apiMenu, PrivateMenuList)
      const subMenu = getFlatMenuItems(permittedMenu)

      // setting private routes
      privateRoutes
        .filter((route) =>
          subMenu.filter((menu) => menu.routeTo).some((menu) => menu.routeTo === route.name)
        )
        .forEach((route) => router.addRoute(home.name, route))
      userStore.setPrivateMenu(permittedMenu)
    } else if (res.status === StatusCode.AuthFailed) {
      // Auth failed & redirect to login
      userStore.setLoggingStatus(false)
      const lang = getLang(to.fullPath)
      return {
        path: `/${lang}/${login.path}`
      }
    } else {
      console.error('get menu failed!!!', res)
    }
    // privateRoutes.forEach((route) => router.addRoute(home.name, route))
    userStore.setLoadedPrivateRoutes(true)
    // redirect to path
    return to.fullPath
  }

  // console.log('current routes', router.getRoutes())
  return true
})

// 产生菜单列表
function genMenu(apiMenuList: Menu[], mainMenuList: MainMenuItem[]): MainMenuItem[] {
  return apiMenuList.map(({ id, p_id, children, title }) => {
    const menuItem = mainMenuList.find(
      (menu) => menu.apiMapping?.id === id && menu.apiMapping.p_id === p_id
    ) as MainMenuItem
    if (!menuItem) {
      console.error(`菜单[${title}] id(${id}) 与 p_id(${p_id}) 不存在`)
    }
    return {
      ...menuItem,
      children: children && menuItem?.children ? genMenu(children, menuItem.children) : undefined
    }
  })
}

// 取得语系
function getLang(fullPath: string) {
  const lang = fullPath
    .split('/')
    .filter((path) => path)
    .at(0)
  return lang && SUPPORT_LANGS.includes(lang) ? lang : DEFAULT_LANG
}

export default router

export * from './constants'
