/** 路由 - 帐号管理 */
const ManageRouteMaps = {
  /** 后台用户 */
  admin: {
    path: 'account-admin',
    name: 'account-admin'
  },
  /** 权限配置 */
  permission: {
    path: 'permission-setting',
    name: 'permission-setting'
  },
  /** 一般会员 */
  member: {
    path: 'account-member',
    name: 'account-member'
  }
} as const

/** 路由 - 运营管理 */
const OperationRouteMaps = {
  /** 绑定申请 */
  bindingApplication: {
    path: 'binding-application',
    name: 'binding-application'
  },
  /** 维权申请 */
  rightsProtectionApplication: {
    path: 'rights-protection-application',
    name: 'rights-protection-application'
  },
  /** 联盟申请 */
  leagueApplication: {
    path: 'league-application',
    name: 'league-application'
  }
} as const

/** 路由 - 广宣管理 */
const AdvertiseRouteMaps = {
  /** 广告设置 */
  adSettings: {
    path: 'ad-settings',
    name: 'ad-settings'
  },
  /** 跑马灯公告 */
  announcement: {
    path: 'announcement-settings',
    name: 'announcement-settings'
  }
} as const

/** 路由 - 客服管理 */
const CustomerRouteMaps = {
  /** 联系方式 */
  contactInfo: {
    path: 'contact-info',
    name: 'contact-info'
  }
} as const

/** 主路由对照表 */
const RootRouteMaps = {
  home: {
    path: '',
    name: 'home',
    children: {
      dashboard: {
        path: 'dashboard',
        name: 'dashboard'
      },
      manage: ManageRouteMaps,
      operation: OperationRouteMaps,
      advertise: AdvertiseRouteMaps,
      customer: CustomerRouteMaps
    }
  },
  login: {
    path: 'login',
    name: 'login'
  },
  register: {
    path: 'register',
    name: 'register'
  },
  about: {
    path: 'about',
    name: 'about'
  },
  notFound: {
    path: '/:catchAll(.*)',
    name: 'notFound'
  }
} as const

export { RootRouteMaps, ManageRouteMaps, OperationRouteMaps, AdvertiseRouteMaps, CustomerRouteMaps }
