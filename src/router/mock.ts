// 假的选单资料
const mockMenu = [
  {
    id: 1,
    title: '帐号管理',
    vue_router: '',
    p_id: -1,
    sort: 1,
    children: [
      {
        id: 2,
        title: '后台用户',
        vue_router: '',
        p_id: 1,
        sort: 1
      },
      {
        id: 4,
        title: '权限设置',
        vue_router: '',
        p_id: 1,
        sort: 3
      }
    ]
  },
  {
    id: 5,
    title: '运营管理',
    vue_router: '',
    p_id: -1,
    sort: 2,
    children: [
      {
        id: 8,
        title: '联盟申请',
        vue_router: '',
        p_id: 5,
        sort: 3
      }
    ]
  },
  {
    id: 9,
    title: '广宣管理',
    vue_router: '',
    p_id: -1,
    sort: 3,
    children: [
      {
        id: 10,
        title: '广告设置',
        vue_router: '',
        p_id: 9,
        sort: 1
      },
      {
        id: 11,
        title: '跑马公告',
        vue_router: '',
        p_id: 9,
        sort: 2
      }
    ]
  },
  {
    id: 12,
    title: '客服管理',
    vue_router: '',
    p_id: -1,
    sort: 4,
    children: [
      {
        id: 13,
        title: '联系方式',
        vue_router: '',
        p_id: 12,
        sort: 1
      }
    ]
  }
]

export { mockMenu }
