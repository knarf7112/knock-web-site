import axios from '@/api/axios'
import type { Menu, LoginReq, LoginRes } from './types'

/** 登入 */
const login = (params: LoginReq) => {
  return axios.post<LoginRes>('/console/login', params)
}

/** 登出 */
const logout = () => {
  return axios.post('/console/logout')
}

/** 取得菜单列表 */
const getMenu = () => {
  return axios.get<Menu[]>('/console/menu')
}

export { getMenu, login, logout }
