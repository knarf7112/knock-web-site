type Menu = {
  id: number
  title: string
  vue_router: string
  p_id: number
  sort: number
  children?: Menu[]
}

/** 登入 Request */
type LoginReq = {
  username: string
  password: string
}

/** 登入 Response */
type LoginRes = {
  id: number // 1
  username: string // "admin"
  nickname: string // "admin"
  password: string // "46f74535c3df838a35e4f1442acf0ccf"
  state: 'active' | 'inactive'
  role_id: number // 1
  created_at: string // "2022-06-23 17:02:47"
  updated_at: string //"2023-05-03 10:20:38"
}

export type { Menu, LoginReq, LoginRes }
