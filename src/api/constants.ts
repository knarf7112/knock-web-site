/** API 代码 */
enum StatusCode {
  /** 成功 */
  Success = '0000',
  /** 失败: cookie失效 */
  AuthFailed = '1005',
  /** 失败: 通用错误 */
  GeneralError = '1000'
}

export { StatusCode }
