import axios from 'axios'
import type { AxiosInstance, AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios'
import { ElMessage } from 'element-plus'
import { useUserStore } from '@/stores'
import type { BaseResponse } from '@/api/types'

const config = {
  baseURL: '/api',
  /** 逾时(ms) */
  timeout: 6000,
  /** 跨域时候允许携带凭证 */
  withCredentials: true
}

class RequestClient {
  service: AxiosInstance
  public constructor(config: AxiosRequestConfig) {
    this.service = axios.create(config)

    /** 请求拦截 */
    this.service.interceptors.request.use(
      (config: AxiosRequestConfig) => {
        //const userStore = useUserStore()

        const customHeaders: any = {}
        // if (userStore.user.info?.token) {
        //   customHeaders['Authorization'] = 'userStore.user.info.token'
        // }
        return {
          ...config,
          headers: {
            ...customHeaders
          }
        }
      },
      (error: AxiosError) => {
        // 请求报错
        Promise.reject(error)
      }
    )

    /** 响应拦截 */
    this.service.interceptors.response.use(
      (response: AxiosResponse) => {
        return response.data
      },
      (error: AxiosError) => {
        const { response } = error
        if (response) {
          this.handleError(response.status)
        }

        if (!window.navigator.onLine) {
          ElMessage.error('网路断线')
        }
      }
    )
  }

  handleError(status: number) {
    switch (status) {
      case 401:
        ElMessage.error('登录失败，请重新登录')
        break
      default:
        ElMessage.error('请求失败')
        break
    }
  }

  // 常用方法
  get<Response = any, Request = any>(
    url: string,
    params?: Request
  ): Promise<BaseResponse<Response>> {
    return this.service.get(url, { params })
  }
  post<Response = any, Request = any>(
    url: string,
    params?: Request
  ): Promise<BaseResponse<Response>> {
    return this.service.post(url, params)
  }
  put<Response = any, Request = any>(
    url: string,
    params?: Request
  ): Promise<BaseResponse<Response>> {
    return this.service.put(url, { params })
  }
  delete<Response = any, Request = any>(
    url: string,
    params?: Request
  ): Promise<BaseResponse<Response>> {
    return this.service.delete(url, { params })
  }
}

export default new RequestClient(config)
