/** 取得 图片路径 */
async function getImage(filePathName: string) {
  const modules = import.meta.glob(['@/assets/images/*.png', '@/assets/images/**/*.png'])
  console.log('all modules', modules)
  return new Promise((res, rej) => {
    const moduleKey = Object.keys(modules).find((key) => key.endsWith(filePathName))
    if (moduleKey) {
      modules[moduleKey]().then((module) => {
        console.log('module', module)
        res((module as any).default)
      })
    } else {
      rej(new Error(`[error] ${filePathName} module not found!!!`))
    }
  })
}

export { getImage }
