/**
 * 取得 localStorage 资料
 * @param key 键值
 * @returns
 */
function getLocalStorage<T = any>(key: string): T | null {
  const data = localStorage.getItem(key)

  if (!data) {
    return null
  }

  try {
    return JSON.parse(data) as T
  } catch (err) {
    console.error('[getLocalStorage] get failed', err)
    return null
  }
}

/**
 * 设定 localStorage 资料
 * @param key 键值
 * @param value
 * @returns
 */
function setLocalStorage<T = any>(key: string, value: T): boolean {
  try {
    localStorage.setItem(key, JSON.stringify(value))
    return true
  } catch (err) {
    console.error('[setLocalStorage] set failed', err)
    return false
  }
}

export { getLocalStorage, setLocalStorage }
