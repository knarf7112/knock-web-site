import { reactive } from 'vue'
import { defineStore } from 'pinia'
import { useCookies } from '@vueuse/integrations/useCookies'
import type { MainMenuItem } from '@/components/Menu'

/** 使用者资讯 */
type User = {
  isLogged?: boolean
  isLoadedPrivateRoutes?: boolean
  privateMenu: MainMenuItem[]
}

/** 使用者资讯 */
const useUserStore = defineStore('user', () => {
  const user = reactive<User>({
    privateMenu: []
  })
  const cookies = useCookies(['CONSOLE_SESSION'])
  const auth = cookies.get('CONSOLE_SESSION')

  if (auth) {
    user.isLogged = true
  }

  /** 设定登入状态 */
  async function setLoggingStatus(login: boolean) {
    user.isLogged = login
    if (!login) {
      cookies.remove('CONSOLE_SESSION', { path: '/' })
      user.isLoadedPrivateRoutes = false
    }
  }

  /** 设定是否载入私有路由 */
  function setLoadedPrivateRoutes(loaded: boolean) {
    user.isLoadedPrivateRoutes = loaded
  }

  /** 配置私有菜单 */
  function setPrivateMenu(menu: MainMenuItem[]) {
    user.privateMenu = menu
  }

  return {
    user,
    setLoggingStatus,
    setLoadedPrivateRoutes,
    setPrivateMenu
  }
})

export { useUserStore }
