import { ref, readonly } from 'vue'
import { defineStore } from 'pinia'
import axios from '@/api/axios'

/** 系统配置 */
const useSystemStore = defineStore('system', () => {
  const systemConfig = ref({})

  async function fetchConfig() {
    console.log('fetch config')
    const config = {
      code: 200,
      msg: 'Success',
      data: {
        name: 'admin',
        maintenance: true
      }
    }

    const response: typeof config = await Promise.resolve(config).then((config) => {
      return new Promise((res) => {
        console.log('fetch test')
        axios.get('system', { type: 'test', id: 123 })
        setTimeout(() => {
          res(config)
        }, 1000)
      })
    })

    console.log('done', response)
    systemConfig.value = response.data
  }

  return {
    systemConfig,
    fetchConfig
  }
})

export { useSystemStore }
