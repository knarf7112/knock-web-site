import { nextTick } from 'vue'
import { createI18n } from 'vue-i18n'
import type { I18nOptions } from 'vue-i18n'

import { setLocalStorage, getLocalStorage } from '@/utils'
import cn from '@/i18n/locales/cn.json'

/** 支援语系 */
enum Language {
  /** 简中 */
  CN = 'cn',
  /** 英文 */
  EN = 'en'
}

/** 支援语系列表 */
const SUPPORT_LANGS: string[] = [Language.CN, Language.EN]
const DEFAULT_LANG: string = Language.CN
const DEFAULT_OPTIONS = {
  locale: getLang(),
  fallbackLocale: Language.CN,
  messages: { cn }
}

/** 取得语系 */
function getLang() {
  const baseUrl = import.meta.env.BASE_URL
  const lang = location.pathname.replace(baseUrl, '').split('/').at(0)

  if (lang && SUPPORT_LANGS.includes(lang)) {
    setLocalStorage('lang', lang)
    return lang
  }

  return getLocalStorage('lang') ?? DEFAULT_LANG
}

/** 初始化多国语系 */
function setupI18n(options: I18nOptions = DEFAULT_OPTIONS) {
  const i18n = createI18n(options)
  setI18nLanguage(i18n, options.locale as Language)
  return i18n
}

/** 设置使用语系 */
function setI18nLanguage(i18n: ReturnType<typeof setupI18n>, locale: Language) {
  if (i18n.mode === 'legacy') {
    i18n.global.locale = locale
  } else {
    ;(i18n.global.locale as any).value = locale
  }

  setLocalStorage('lang', locale)
  document.querySelector('html')?.setAttribute('lang', locale)
}

/** 下载语系檔 */
async function loadLocaleMessage(i18n: ReturnType<typeof setupI18n>, locale: Language) {
  // load locale message with dynamic import
  const messages = await import(
    /** webpackChunkName: "locale-[request]" */ `./locales/${locale}.json`
  )

  // set locale and locale message
  i18n.global.setLocaleMessage(locale, messages.default)

  return nextTick()
}

const i18n = setupI18n()

export default i18n
export { Language, setupI18n, setI18nLanguage, loadLocaleMessage, SUPPORT_LANGS, DEFAULT_LANG }
