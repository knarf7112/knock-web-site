#ref: https://www.docker.com/blog/how-to-use-the-official-nginx-docker-image/
FROM node:latest as build

# 變更工作目錄
WORKDIR /app

COPY ./package.json /app/package.json
COPY ./package-lock.json /app/package-lock.json

RUN npm install
COPY . .
RUN npm run build:dev

FROM nginx
COPY ./.nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build /app/dist /usr/share/nginx/html
RUN ls -l /usr/share/nginx/html
RUN echo '[run done].'

